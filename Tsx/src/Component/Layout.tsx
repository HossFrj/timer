import React from 'react';
import Timer from "./Timer/Timer";

function Layout() : React.FC {
    return (
        <div>
            <Timer/>
        </div>
    );
}

export default Layout;
