import React, {useRef, useState} from 'react';
import './Timer.css'

const Timer:React.FC = () => {
    const [add1, setAdd1] = useState<number>(1);
    const [add2, setAdd2] = useState<number>(1);
    const [check1, setCheck1] = useState<number>(0)
    const [check2, setCheck2] = useState<number>(0)
    const [min1, setMin1] = useState<number>(1);
    const [min2, setMin2] = useState<number>(1);
    const increment1 = useRef<any>(null);
    const increment2 = useRef<any>(null);


    const Add = () => {
        setCheck1(check1 + 1)
        if (check1 === 0) {
            setCheck2(0)
            increment1.current = setInterval(() => {
                setAdd1((add1) => add1 - 1)
            }, 1000)
            // increment3.current = setInterval(() => {
            //     setMin1((min1) => min1 - 1)
            // }, 60000)
            clearInterval(increment2.current)
            // clearInterval(increment4.current)

        } else {

        }
    }
    const Del = () => {
        setCheck2(check2 + 1)
        if (check2 === 0) {
            setCheck1(0)
            increment2.current = setInterval(() => {
                setAdd2((add2) => add2 - 1)
            }, 1000)
            // increment4.current = setInterval(() => {
            //     setMin2((min2) => min2 - 1)
            // }, 60000)
            clearInterval(increment1.current)
            // clearInterval(increment3.current)

        } else {

        }

    }

    if (min1 >= 0) {
    } else {
        setMin1(0)
    }
    if (min2 >= 0) {

    } else {
        setMin2(0)
    }
    if (add1 === -1) {
        if (min1===0){
            setAdd1(0)
        }else {
            setAdd1(59)
            setMin1(min1 - 1)
        }
    } else {

    }
    if (add2 === -1) {
        if (min2===0){
            setAdd2(0)
        }else {
            setAdd2(59)
            setMin2(min2 - 1)
        }
    } else {

    }
    return (
        <div className={"main"}>
            <div className={"T1"}>
                <div className={"T11"}>
                    <button onClick={Add} className={"btn1"}>1</button>
                    <button onClick={Del} className={"btn2"}>2</button>
                </div>
                <div className={"T12"}>
                    <span className={"spa1"}>{min2 + ':' + add2}</span>
                    <span className={"spa2"}>{min1 + ':' + add1}</span>
                </div>
            </div>
        </div>
    );
}

export default Timer;